﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var count = Data.Count();
            Data = Data.Where(x => x.Id != id).ToList();
            return Task.FromResult(count != Data.Count());
        }

        public Task<Guid?> AddAsync(T entity)
        {
            if (!Data.Any(x => x.Id == entity.Id))
            {
                var count = Data.Count();
                var DataList = Data.ToList();
                DataList.Add(entity);
                Data = DataList;
                return Task.FromResult<Guid?>(count != Data.Count() ? entity.Id : null);
            }
            return Task.FromResult<Guid?>(null);
        }

        public Task<Guid?> UpdateAsync(T entity)
        {
            if (Data.Any(x => x.Id == entity.Id))
            {
                var count = Data.Count();
                var DataList = Data.ToList();
                var dataEntity = DataList.First(x => x.Id == entity.Id);
                DataList.Remove(dataEntity);
                DataList.Add(entity);
                Data = DataList;

                return Task.FromResult<Guid?>(count == Data.Count() ? entity.Id : null);
            }
            return Task.FromResult<Guid?>(null);
        }
    }
}