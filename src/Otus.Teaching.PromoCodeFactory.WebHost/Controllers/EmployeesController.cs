﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var result = await _employeeRepository.DeleteByIdAsync(id);
            if (result)
                return Ok();

            return BadRequest();
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Guid?>> CreateEmployeeAsync(EmployeeRequest request)
        {
            List<Role> roles = await CreateRoles(request);

            var employeeEntity = new Employee(request.FirstName, request.LastName, request.Email, request.AppliedPromocodesCount, roles);

            var result = await _employeeRepository.AddAsync(employeeEntity);
            if (result.HasValue)
                return result;

            return BadRequest();
        }

        [HttpPost("{id:guid}")]
        [Route("Update")]
        public async Task<ActionResult<Guid?>> UpdateEmployeeAsync([FromHeader]Guid id, [FromBody]EmployeeRequest request)
        {
            List<Role> roles = await CreateRoles(request);

            var employeeEntity = new Employee(id, request.FirstName, request.LastName, request.Email, request.AppliedPromocodesCount, roles);

            var result = await _employeeRepository.UpdateAsync(employeeEntity);
            if (result.HasValue)
                return result;

            return BadRequest();
        }

        private async Task<List<Role>> CreateRoles(EmployeeRequest request)
        {
            var roles = new List<Role>();
            if (request.Roles != null)
            {
                foreach (var item in request.Roles)
                {
                    var role = await _roleRepository.GetByIdAsync(item.Id);
                    if (role == null)
                    {
                        role = new Role
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Description = item.Description
                        };
                        await _roleRepository.AddAsync(role);
                    }
                    roles.Add(role);
                }
            }

            return roles;
        }
    }
}